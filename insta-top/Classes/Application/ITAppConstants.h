//
//  ITAppConstants.h
//  insta-top
//
//  Created by Denis Shalagin on 02/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

@interface ITAppConstants : NSObject

// Instagram
FOUNDATION_EXPORT NSString *const kITInstagramAPI;
FOUNDATION_EXPORT NSString *const kITInstagramClientId;

@end
