//
//  ITManagedEntity.h
//  insta-top
//
//  Created by Denis Shalagin on 8/3/15.
//  Copyright (c) 2015 Maxiosoftware. All rights reserved.
//

@interface ITManagedEntity : NSManagedObject

+ (id)createEntity;
+ (id)createEntityWithJSONDictionary:(NSDictionary *)dictionary;

+ (NSArray *)fetchAll;
+ (void)deleteAll;

+ (void)saveContext;

@end
