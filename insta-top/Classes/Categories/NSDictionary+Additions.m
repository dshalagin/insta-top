//
//  NSDictionary+Additions.m
//  insta-top
//
//  Created by Denis Shalagin on 03/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (id)safeObjectForKeyPath:(id)keyPath {
    NSObject *object = [self valueForKeyPath:keyPath];
    
    if (object == [NSNull null]) {
        return nil;
    }
    
    return object;
}

@end
