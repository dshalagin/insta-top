//
//  UIView+Additions.h
//  insta-top
//
//  Created by Denis Shalagin on 02/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

@interface UIView (Additions)

@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

@end
