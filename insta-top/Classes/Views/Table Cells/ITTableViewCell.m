//
//  ITTableViewCell.m
//  insta-top
//
//  Created by Denis Shalagin on 03/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

#import "ITTableViewCell.h"

@implementation ITTableViewCell

#pragma mark - Static methods

+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object {
    return 44.0f;
}

#pragma mark - Initialization

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    return self;
}

@end
