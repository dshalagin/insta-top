//
//  ITMediaManager.h
//  insta-top
//
//  Created by Denis Shalagin on 8/3/15.
//  Copyright (c) 2015 Maxiosoftware. All rights reserved.
//

@protocol ITMediaManagerDelegate;

//----------------------------------------------------------------------
@interface ITMediaManager : NSObject

@property (nonatomic, weak) id<ITMediaManagerDelegate> delegate;

+ (instancetype)sharedManager;

- (void)downloadFilesAtURLs:(NSArray *)urls; // urls - an array of url strings
- (void)cancelDownloading;
- (void)clearStorage;

- (NSString *)localFileForURLString:(NSString *)urlString;

@end


//----------------------------------------------------------------------
@protocol ITMediaManagerDelegate <NSObject>

- (void)mediaManagerDidStartLoad:(ITMediaManager *)mediaManager;
- (void)mediaManager:(ITMediaManager *)mediaManager didLoadFiles:(NSUInteger)loadedFiles totalFiles:(NSUInteger)totalFiles;
- (void)mediaManagerDidFinishLoad:(ITMediaManager *)mediaManager;
- (void)mediaManagerDidCancel:(ITMediaManager *)mediaManager;

@end
