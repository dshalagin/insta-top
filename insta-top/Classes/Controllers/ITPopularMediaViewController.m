//
//  ITPopularMediaViewController.m
//  insta-top
//
//  Created by Denis Shalagin on 02/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

#import "ITPopularMediaViewController.h"
#import "ITMedia.h"
#import "ITMediaManager.h"
#import "ITMediaTableCell.h"
#import "ITPopularMediaModel.h"

//----------------------------------------------------------------------
@interface ITPopularMediaViewController () <UITableViewDataSource, UITableViewDelegate, ITMediaManagerDelegate>

@end


//----------------------------------------------------------------------
@implementation ITPopularMediaViewController {
    // view
    UIActivityIndicatorView *_indicatorView;
    UILabel *_indicatorInfoLabel;
    
    UITableView *_tableView;
    
    // model
    ITPopularMediaModel *_model;
    ITMediaManager *_mediaManager;
    
    // data
    NSArray *_items;
}

#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        _model = [ITPopularMediaModel model];
        
        _mediaManager = [ITMediaManager sharedManager];
        _mediaManager.delegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadModel)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - View lifecycle

- (void)loadView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [view addSubview:_tableView];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view addSubview:_indicatorView];
    
    _indicatorInfoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _indicatorInfoLabel.font = [UIFont systemFontOfSize:14.0f];
    _indicatorInfoLabel.textColor = [UIColor lightGrayColor];
    _indicatorInfoLabel.textAlignment = NSTextAlignmentCenter;
    _indicatorInfoLabel.hidden = YES;
    [view addSubview:_indicatorInfoLabel];
    
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Popular Media", @"Popular media screen title.");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self reloadModel];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _tableView.frame = self.view.bounds;
    _indicatorView.center = self.view.center;
    _indicatorInfoLabel.frame = CGRectMake(0,
                                           _indicatorView.bottom + 5.f,
                                           self.view.width,
                                           _indicatorInfoLabel.font.pointSize + 2.f);
}


#pragma mark - Model

- (void)reloadModel {
    // show loading indicator
    [self showLoading];
    
    // update loading state info
    _indicatorInfoLabel.text = NSLocalizedString(@"Loading data...", nil);
    
    // load model
    [_model loadModelWithSuccess:^(NSArray *objects) {
        // store items
        _items = objects;
        
        // clear storage
        [_mediaManager clearStorage];
        
        // load new files
        [_mediaManager downloadFilesAtURLs:[objects valueForKeyPath:@"standartResolutionURLString"]];
    } failure:^(NSError *error) {
        // load cache
        [self loadCachedData];
        
        // remove loading indicator
        [self hideLoading];
        
        // show error if no data
        if (!_items) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error alert title.")
                                        message:[error localizedDescription]
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title.")
                              otherButtonTitles:nil] show];
        }
    }];
}

- (void)loadCachedData {
    _items = [ITMedia fetchAll];
    
    [_tableView reloadData];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MediaTableCell";
    ITMediaTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[ITMediaTableCell alloc] initWithReuseIdentifier:CellIdentifier];
    }
    
    cell.object = _items[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ITMediaTableCell calculateHeightForWidth:tableView.width withObject:_items[indexPath.row]];
}


#pragma mark - ITMediaManagerDelegate

- (void)mediaManagerDidStartLoad:(ITMediaManager *)mediaManager {
    // update loading state info
    _indicatorInfoLabel.text = NSLocalizedString(@"Downloading media...", nil);
}

- (void)mediaManager:(ITMediaManager *)mediaManager didLoadFiles:(NSUInteger)loadedFiles totalFiles:(NSUInteger)totalFiles {
    // update loading state info
    _indicatorInfoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Downloading media %lu of %lu", nil), loadedFiles, totalFiles];;
}

- (void)mediaManagerDidFinishLoad:(ITMediaManager *)mediaManager {
    [self hideLoading];
    
    // scroll to top
    [_tableView scrollRectToVisible:CGRectMake(0, 0, 1.f, 1.f) animated:NO];
    
    // reload data
    [_tableView reloadData];
}

- (void)mediaManagerDidCancel:(ITMediaManager *)mediaManager {
    // nothing to do since download is cancelled
}


#pragma mark - Loading state

- (void)showLoading {
    [_indicatorView startAnimating];
    _indicatorInfoLabel.hidden = NO;
    
    _tableView.hidden = YES;
}

- (void)hideLoading {
    [_indicatorView stopAnimating];
    _indicatorInfoLabel.hidden = YES;
    
    _tableView.hidden = NO;
}

@end
