//
//  ITPopularMediaModel.m
//  insta-top
//
//  Created by Denis Shalagin on 02/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

#import "ITPopularMediaModel.h"
#import "ITMedia.h"
#import "ITMediaManager.h"

@implementation ITPopularMediaModel

#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        self.resourcePath = @"/media/popular";
        self.entityClass = [ITMedia class];
        self.appendNewData = NO;
    }
    
    return self;
}

@end
