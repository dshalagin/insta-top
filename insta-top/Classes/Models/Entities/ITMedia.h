//
//  ITMedia.h
//
//
//  Created by Denis Shalagin on 8/3/15.
//
//

#import "ITManagedEntity.h"

@interface ITMedia : ITManagedEntity

@property (nonatomic, strong) NSNumber *mediaId;
@property (nonatomic, strong) NSString *captionText;
@property (nonatomic, strong) NSString *standartResolutionURLString;

@end
