//
//  ITMediaManager.m
//  insta-top
//
//  Created by Denis Shalagin on 8/3/15.
//  Copyright (c) 2015 Maxiosoftware. All rights reserved.
//

#import "ITMediaManager.h"

#import <libkern/OSAtomic.h>

@implementation ITMediaManager {
    NSOperationQueue *_queue;
}

#pragma mark - Initialization

+ (instancetype)sharedManager {
    static ITMediaManager *_mediaManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _mediaManager = [[self alloc] init];
    });
    
    return _mediaManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 4;
    }
    
    return self;
}


#pragma mark - Public methods

- (void)downloadFilesAtURLs:(NSArray *)urls {
    // cancel  previously started operations
    [self cancelDownloading];
    
    // notify delegate
    [self.delegate mediaManagerDidStartLoad:self];
    
    // create completion handler
    NSBlockOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.delegate mediaManagerDidFinishLoad:self];
        }];
    }];
    
    int32_t total = (int32_t)[urls count];
    __block volatile int32_t completed = 0;
    
    for (NSString *urlString in urls) {
        NSBlockOperation *operation = [[NSBlockOperation alloc] init];
        __weak NSBlockOperation *weakOperation = operation;
        [operation addExecutionBlock:^{
            if (![weakOperation isCancelled]) {
                NSURL *fileURL = [NSURL URLWithString:urlString];
                NSString *localFilePath = [self localFilePathForURL:fileURL];
                
                // download file locally
                NSData *fileData = [NSData dataWithContentsOfURL:fileURL];
                [fileData writeToFile:localFilePath atomically:YES];
                
                // prevent file from backup
                [self addSkipBackupAttributeToItemAtPath:localFilePath];
                
                OSAtomicIncrement32(&completed);
                
                // notify delegate
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.delegate mediaManager:self didLoadFiles:completed totalFiles:total];
                }];
            }
        }];
        [completionOperation addDependency:operation];
    }
    
    [_queue addOperations:completionOperation.dependencies waitUntilFinished:NO];
    [_queue addOperation:completionOperation];
}

- (void)cancelDownloading {
    if ([_queue operationCount] > 0) {
        [_queue cancelAllOperations];
        [self.delegate mediaManagerDidCancel:self];
    }
}

- (void)clearStorage {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *directory = [self mediaDirectory];
    NSError *error = nil;
    for (NSString *file in [fileManager contentsOfDirectoryAtPath:directory error:&error]) {
        BOOL success = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error) {
            NSLog(@"Error deleting media file %@ : %@", file, [error localizedDescription]);
        }
    }
}

- (NSString *)localFileForURLString:(NSString *)urlString {
    NSString *filePath = [self localFilePathForURL:[NSURL URLWithString:urlString]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // check if file exist
        return nil;
    }
    
    return filePath;
}

#pragma mark - Private

- (NSString *)mediaDirectory {
    NSString *documentsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [documentsDirectory stringByAppendingPathComponent:@"Media/"];
}

- (NSString *)localFilePathForURL:(NSURL *)url {
    return [[self mediaDirectory] stringByAppendingString:[url lastPathComponent]];
}

/*
 Prevent files from being backed up to iCloud and iTunes
 https://developer.apple.com/library/prerelease/ios/qa/qa1719/_index.html
 */
- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)filePath {
    NSURL *url = [NSURL fileURLWithPath:filePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:url.path]) {
        NSError *error = nil;
        BOOL success = [url setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:&error];
        if (success) {
            return YES;
        }
        
        NSLog(@"Error excluding %@ from backup %@", [url lastPathComponent], error);
    }
    
    return NO;
}

@end
