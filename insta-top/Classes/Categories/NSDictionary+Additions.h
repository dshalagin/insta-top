//
//  NSDictionary+Additions.h
//  insta-top
//
//  Created by Denis Shalagin on 03/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

@interface NSDictionary (Additions)

- (id)safeObjectForKeyPath:(id)keyPath;

@end
