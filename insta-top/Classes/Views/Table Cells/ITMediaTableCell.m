//
//  ITMediaTableCell.m
//  insta-top
//
//  Created by Denis Shalagin on 03/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

#import "ITMediaTableCell.h"
#import "ITMedia.h"
#import "ITMediaManager.h"

#define kCaptionTextOffset 5.0f
#define kCaptionTextFont [UIFont systemFontOfSize:18.0f]

@implementation ITMediaTableCell

+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object {
    assert([object isKindOfClass:[ITMedia class]]);
    ITMedia *media = (ITMedia *)object;
    
    CGFloat height = 0;
    
    // apply image height
    height += width;
    
    // apply text height
    height += kCaptionTextOffset;
    height += [self labelHeightForWidth:(width - (2 * kCaptionTextOffset)) withText:media.captionText];
    height += 2 * kCaptionTextOffset; // bottom offset
    
    return height;
}

+ (CGFloat)labelHeightForWidth:(CGFloat)width withText:(NSString *)text {
    CGRect textFrame = [text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{ NSFontAttributeName : kCaptionTextFont }
                                          context:nil];
    return textFrame.size.height;
}


#pragma mark - Initialization

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        // disable selection
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // allow multiple lines
        self.textLabel.numberOfLines = 0;
        
        // apply image scale mode
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    return self;
}


#pragma mark - View lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(0, 0, self.contentView.width, self.contentView.width);
    
    CGFloat textWidth = self.contentView.width - (2 * kCaptionTextOffset);
    self.textLabel.frame = CGRectMake(kCaptionTextOffset,
                                      self.imageView.bottom + kCaptionTextOffset,
                                      textWidth,
                                      [ITMediaTableCell labelHeightForWidth:textWidth withText:self.textLabel.text]);
}

#pragma mark - ITTableViewCell

- (void)setObject:(id)object {
    [super setObject:object];
    
    assert([object isKindOfClass:[ITMedia class]]);
    ITMedia *media = (ITMedia *)object;
    
    self.textLabel.text = media.captionText;
    
    NSString *imageFile = [[ITMediaManager sharedManager] localFileForURLString:media.standartResolutionURLString];
    self.imageView.image = [UIImage imageWithContentsOfFile:imageFile];
}

@end
