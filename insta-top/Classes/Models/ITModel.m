//
//  ITModel.m
//  insta-top
//
//  Created by Denis Shalagin on 02/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

#import "ITModel.h"
#import "ITManagedEntity.h"

@implementation ITModel

#pragma mark - Initialization

+ (id)model {
    return [[self alloc] init];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _queryItems = [NSArray array];
    }
    
    return self;
}


#pragma mark - Public

- (void)loadModelWithSuccess:(void (^)(NSArray *))success failure:(void (^)(NSError *))failure {
    // apply client id param
    NSMutableArray *queryItems = [self.queryItems mutableCopy];
    [queryItems addObject:[NSURLQueryItem queryItemWithName:@"client_id" value:kITInstagramClientId]];
    
    // create url components
    NSString *urlString = [kITInstagramAPI stringByAppendingString:self.resourcePath];
    NSURLComponents *components = [NSURLComponents componentsWithString:urlString];
    components.queryItems = queryItems;
    
    // create and perform request
    NSURLRequest *request = [NSURLRequest requestWithURL:components.URL];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        // check network error
        if (connectionError) {
            [self notifyFailure:failure withError:[connectionError localizedDescription] errorCode:[connectionError code]];
            return;
        }
        
        // parse json response
        NSError *error;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&error];
        if (error) {
            [self notifyFailure:failure withError:[error localizedDescription] errorCode:[error code]];
            return;
        }
        
        // parse data
        NSArray *objects = responseDict[@"data"];
        if (!objects) {
            [self notifyFailure:failure withError:NSLocalizedString(@"Can't parse response.", nil) errorCode:-1];
            return;
        }
        
        // delete previous if needed
        if (!self.appendNewData) {
            [self.entityClass deleteAll];
        }
        
        // create entities
        NSMutableArray *entities = [[NSMutableArray alloc] initWithCapacity:[objects count]];
        for (NSDictionary *object in objects) {
            id entity = [self.entityClass createEntityWithJSONDictionary:object];
            [entities addObject:entity];
        }
        
        // save context
        [ITManagedEntity saveContext];
        
        [self notifySuccess:success withObjects:[NSArray arrayWithArray:entities]];
    }];
}


#pragma mark - Private Helpers

- (void)notifyFailure:(void (^)(NSError *))failure withError:(NSString *)errorValue errorCode:(NSInteger)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
        [userInfo setObject:errorValue forKey:NSLocalizedFailureReasonErrorKey];
        failure([NSError errorWithDomain:@"IT" code:errorCode userInfo:userInfo]);
    });
}

- (void)notifySuccess:(void (^)(NSArray *))success withObjects:(NSArray *)objects {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        success(objects);
    });
}

@end
