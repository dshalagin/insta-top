//
//  ITMedia.m
//
//
//  Created by Denis Shalagin on 8/3/15.
//
//

#import "ITMedia.h"

@implementation ITMedia

@dynamic mediaId;
@dynamic captionText;
@dynamic standartResolutionURLString;

+ (id)createEntityWithJSONDictionary:(NSDictionary *)dictionary {
    ITMedia *entity = [self createEntity];
    entity.mediaId = [NSNumber numberWithInt:[dictionary[@"id"] intValue]];
    entity.captionText = [dictionary safeObjectForKeyPath:@"caption.text"];
    entity.standartResolutionURLString = [dictionary safeObjectForKeyPath:@"images.standard_resolution.url"];
    
    return entity;
}

@end
