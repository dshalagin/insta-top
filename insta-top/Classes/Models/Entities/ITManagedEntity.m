//
//  ITManagedEntity.m
//  insta-top
//
//  Created by Denis Shalagin on 8/3/15.
//  Copyright (c) 2015 Maxiosoftware. All rights reserved.
//

#import "ITManagedEntity.h"
#import "AppDelegate.h"

@implementation ITManagedEntity

#pragma mark - Initialization

+ (id)createEntity {
    NSManagedObjectContext *applicationContext = [self applicationContext];
    NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:applicationContext];
    
    return entity;
}

+ (id)createEntityWithJSONDictionary:(NSDictionary *)dictionary {
    return [self createEntity];
}

+ (NSArray *)fetchAll {
    NSManagedObjectContext *applicationContext = [self applicationContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                              inManagedObjectContext:applicationContext];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSArray *results = [applicationContext executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        return nil;
    }
    
    return results;
}

+ (void)deleteAll {
    NSManagedObjectContext *applicationContext = [self applicationContext];
    NSArray *entities = [self fetchAll];
    for (id entity in entities) {
        [applicationContext deleteObject:entity];
    }
    
    [self saveContext];
}

+ (void)saveContext {
    NSManagedObjectContext *applicationContext = [self applicationContext];
    [applicationContext save:nil];
}

#pragma mark - Private helpers

+ (NSManagedObjectContext *)applicationContext {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

@end
