//
//  ITAppConstants.m
//  insta-top
//
//  Created by Denis Shalagin on 02/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

#import "ITAppConstants.h"

@implementation ITAppConstants

// Instagram
NSString *const kITInstagramAPI = @"https://api.instagram.com/v1";
NSString *const kITInstagramClientId = @"3d3f8254f39f414498d2b1ba6b0c294d";

@end
