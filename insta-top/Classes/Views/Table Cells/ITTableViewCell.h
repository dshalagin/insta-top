//
//  ITTableViewCell.h
//  insta-top
//
//  Created by Denis Shalagin on 03/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

@interface ITTableViewCell : UITableViewCell

@property (nonatomic, strong) id object;


+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object;

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
