//
//  ITModel.h
//  insta-top
//
//  Created by Denis Shalagin on 02/08/15.
//  Copyright © 2015 Maxiosoftware. All rights reserved.
//

@interface ITModel : NSObject

@property (nonatomic, strong) NSString *resourcePath;
@property (nonatomic, strong) NSArray *queryItems; // an array of NSURLQueryItem objects
@property (nonatomic, assign) Class entityClass;
@property (nonatomic, assign) BOOL appendNewData; // set whether new entities shoul be appended or rewrited

+ (id)model;

- (void)loadModelWithSuccess:(void (^)(NSArray *objects))success failure:(void (^)(NSError *error))failure;

@end
